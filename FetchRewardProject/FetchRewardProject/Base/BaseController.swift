//
//  BaseController.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import UIKit

///the base controller for a ll the controllers in the app

open class BaseController: UIViewController {
    
    /// The baseView of controller
    let baseView: BaseView
    
    /// The baseViewModel of controller
    public let baseViewModel: BaseViewModel
    
    /// The backButton
    lazy var backButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(systemName: "back")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backButtonClicked))//UIBarButtonItem(image: .back, style: .plain, target: self, action: #selector(backButtonClicked))
        return button
    }()
    
    
    /// when view is loaded
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        /// setup UI
        setupUI()
        
        /// observe events
        observeEvents()
        
        ///setupNavigation bar
        setupNavigationBar()
    }

    /// Initializer for a controller
    /// - Parameters:
    ///   - baseView: the view associated with the controller
    ///   - baseViewModel: viewModel associated with the controller
    init(baseView: BaseView, baseViewModel: BaseViewModel) {
        self.baseView = baseView
        self.baseViewModel = baseViewModel
        super.init(nibName: nil, bundle: nil)
    }
    /// Not available
    required public init?(coder: NSCoder) {
        fatalError("Controller should never be instantiated from coder")
    }
    
    
    /// Load the base view as the view of controller
    override open func loadView() {
        super.loadView()
        view = baseView
    }
    
    /// setup trigger
    func setupUI() {}
    
    //setup navigation bar
    func setupNavigationBar() {}
    
    func observeEvents() {}
    
    // makes navigation bar translucen. After making the nav bar translucent, make sure to recal this function in "viewWillDisappear" with false so that navbar of other controller are not affected
     /// - Parameter status: true to make translucent, false to revert back to app default
     func makeNavBarTranslucent(status: Bool) {
         if status {
             navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
             navigationController?.navigationBar.isTranslucent = true
         } else {
             navigationController?.navigationBar.isTranslucent = false
             navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
         }
     }
    
    
    /// When backbutton is clicked
    @objc func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// Deint call check
    deinit {
        debugPrint("De-Initialized --> \(String(describing: self))")
    }
}

extension BaseController {
    
    /// Method to present alert with actions provided
    /// - Parameters:
    ///   - title: the title of alert
    ///   - msg: the message of alert
    ///   - actions: the actions to display
    ///   - titleAttributes: attributes to be display for title of alert
    ///   - messageAttribute: attributes for the message of alert
    ///   - completion: action completion handler
    open func alert(title: String, msg: String, actions: [AlertActionable], titleAttributes: [NSAttributedString.Key: Any]? = nil, messageAttribute: [NSAttributedString.Key: Any]? = nil, completion: ((_ action: AlertActionable) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        if let attributes = titleAttributes {
            //attributed title
            let attributedTitle = NSAttributedString(string: title, attributes: attributes)
            alert.setValue(attributedTitle, forKey: "attributedTitle")
        }
        
        if let attributes = messageAttribute {
            /// Attributed message
            let attributedMsg = NSAttributedString(string: msg, attributes: attributes)
            alert.setValue(attributedMsg, forKey: "attributedMessage")
        }
        
        actions.forEach { action in
            let alertAction = UIAlertAction(title: action.title, style: action.style) { _ in
                completion?(action)
            }
            alert.addAction(alertAction)
        }
        present(alert, animated: true, completion: nil)
    }
}

/// To inherit
extension UIViewController: Presentable {
    public var presenting: UIViewController? {
        return self
    }
}
