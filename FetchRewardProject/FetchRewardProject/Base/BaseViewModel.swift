//
//  BaseViewModel.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import Combine

///This viewmodel will act as the base viewmodel for every viewmodel that will be created
open class BaseViewModel {
    
    ///The cleanup bag similar to dispose bag in rx
    public var bag = Set<AnyCancellable>()
    
    ///Trigger for the route
    public var trigger = PassthroughSubject<AppRoutable, Never>()
    
    ///initializer
    public init() { }
    
    ///Deinit call cehcks
    deinit {
        debugPrint("De-Initialized --> \(String(describing: self))")
    }
}
