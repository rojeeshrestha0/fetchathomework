//
//  CategoryCoordinator.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import Combine

final class CategoryCoordinator: BaseCoordinator {
    
    ///The main app route
    private let route: Route
    
    ///Initializer
    init(route: Route) {
        self.route = route
        super.init()
    }
    
    /// Method that will start the initial app flow
    /// - Parameter deepLink: the deeplink if needed
    override func start(with deepLink: Deeplink?) {
        if deepLink == nil {
            showCategory()
        } else {
            switch deepLink! {
            default: break
            }
        }
    }
    
    /// show the home screen
    private func showCategory() {
        /// instantiate
        let view = CategoryView()
        let viewModel = CategoryViewModel()
        let controller = CategoryController(baseView: view, baseViewModel: viewModel)

        viewModel.trigger.receive(on: RunLoop.main).sink { [weak self] (route) in
            guard let self = self else { return }
           self.handleRouteTrigger(route)
        }.store(in: &viewModel.bag)
        route.setRoot(controller)
    }
    
    ///shoe the mealList under the category
    private func showMealList(categoryName: String) {
        let view = MealListView()
        let viewModel = MealListViewModel(categoryName: categoryName)
        let controller = MealListController(baseView: view, baseViewModel: viewModel)

        viewModel.trigger.receive(on: RunLoop.main).sink { [weak self] (route) in
            guard let self = self else { return }
           self.handleRouteTrigger(route)
        }.store(in: &viewModel.bag)
        route.push(controller, animated: true)
    }
    
    ///shoe the mealList under the category
    private func showMealDetails(mealId: String) {
        let view = MealDetailsView()
        let viewModel = MealDetailsViewModel(mealId: mealId)
        let controller = MealDetailsController(baseView: view, baseViewModel: viewModel)

        viewModel.trigger.receive(on: RunLoop.main).sink { [weak self] (route) in
            guard let self = self else { return }
           self.handleRouteTrigger(route)
        }.store(in: &viewModel.bag)
        route.push(controller, animated: true)
    }
    
    
    
    /// Handles the trigger for app routing
    /// - Parameter trigger: the routable trigger
    private func handleRouteTrigger(_ trigger: AppRoutable) {
        switch trigger {
        case AppRoute.category:
            showCategory()
        case AppRoute.mealList(let categoryName):
            showMealList(categoryName: categoryName)
        case AppRoute.mealDetails(let mealId):
            showMealDetails(mealId: mealId)
        default: break
        }
    }
    
}
