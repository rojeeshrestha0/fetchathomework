//
//  CategoryViewModel.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import Combine

class CategoryViewModel: BaseViewModel {
    let manager: MealCategoryManager
    
    override init() {
            manager = MealCategoryManager()
    }
    
    func callCategoryApi() {
        manager.request(router: MealRouter.categories)
    }
}
