//
//  CategoryCell.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import UIKit
import Kingfisher

protocol Selection: class {
}

class CategoryCell: UICollectionViewCell {
    //Image of the category
    lazy var categoryImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        return imageView
    }()
    
    ///The name label of fthe category
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 0
        return label
    }()
    
    ///The description of the content for the category
    lazy var descrptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
//        label.backgroundColor = .red
        return label
    }()
    
    var categoryModel: MealCategory! {
        didSet {
            configureCell()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func create() {
        contentView.backgroundColor = .clear
        contentView.addSubview(categoryImage)
        contentView.addSubview(nameLabel)
        contentView.addSubview(descrptionLabel)
        
        
        NSLayoutConstraint.activate([
        ///constraint for image
            categoryImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            categoryImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            categoryImage.heightAnchor.constraint(equalToConstant: 100.0),
            categoryImage.widthAnchor.constraint(equalToConstant: 100.0),
            
            
            nameLabel.topAnchor.constraint(equalTo: categoryImage.topAnchor, constant: -2.0),
            nameLabel.leadingAnchor.constraint(equalTo: categoryImage.trailingAnchor, constant: 20.0),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            nameLabel.heightAnchor.constraint(equalToConstant: 20.0),
            
            descrptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor),
            descrptionLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),
            descrptionLabel.trailingAnchor.constraint(equalTo: nameLabel.trailingAnchor),
            descrptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -2.0)
        ])
    }
    
    private func configureCell() {
   
        nameLabel.text = categoryModel.categoryName
        descrptionLabel.text = categoryModel.categoryDescription.stripped
        categoryImage.setImage(string: categoryModel.categoryImage)//"https://example.com/image.png")
    }
}
