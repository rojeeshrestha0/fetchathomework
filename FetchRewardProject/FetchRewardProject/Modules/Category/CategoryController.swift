//
//  CategoryController.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import UIKit

class CategoryController: BaseController {
    /// The view
    private lazy var screenView: CategoryView = {
        return self.baseView as! CategoryView //swiftlint:disable:this force_cast
    }()
    
    /// The viewModel
    private lazy var viewModel: CategoryViewModel = {
        return self.baseViewModel as! CategoryViewModel //swiftlint:disable:this force_cast
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        screenView.collectionView.delegate = self
        screenView.collectionView.dataSource = self
        viewModel.callCategoryApi()
        screenView.indicate = true

    }
    
    override func setupNavigationBar() {
        navigationItem.title = LocalizedKey.categoryTitle.value
    }
    
    //Shows navigationbar only on this controller
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func observeEvents() {
        viewModel.manager.$categories.receive(on: RunLoop.main).sink{ [weak self] _ in
            self?.screenView.collectionView.reloadData()
            self?.screenView.indicate = false
        }.store(in: &viewModel.bag)
    }
}

extension CategoryController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       viewModel.manager.categories.count
}

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.identifier, for: indexPath) as! CategoryCell
       cell.categoryModel = viewModel.manager.categories[indexPath.row]
       return cell
   }

   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       if let category = viewModel.manager.categories[indexPath.row] as? MealCategory{
           
           let approximateDescriptionWidth = view.frame.width - 10 - 100 - 10
           let size = CGSize(width: approximateDescriptionWidth, height: 1000)
           let estimatedFrame = NSString(string: category.categoryDescription).boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)
           return CGSize(width: view.frame.width, height: estimatedFrame.height +  100)
       }

       return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height*0.25)
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        guard let category = viewModel.manager.categories[indexPath.row] as? MealCategory else {return}
        viewModel.trigger.send(AppRoute.mealList(category.categoryName))
            
        }
}
