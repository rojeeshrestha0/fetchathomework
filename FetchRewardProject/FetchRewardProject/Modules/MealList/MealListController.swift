//
//  MealListController.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

import Foundation
import UIKit

class MealListController: BaseController {
    private lazy var screenView: MealListView = {
        return self.baseView as! MealListView //swiftlint:disable:this force_cast
    }()
    
    /// The viewModel
    private lazy var viewModel: MealListViewModel = {
        return self.baseViewModel as! MealListViewModel //swiftlint:disable:this force_cast
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenView.collectionView.delegate = self
        screenView.collectionView.dataSource = self
        viewModel.callMealListApi()
        screenView.indicate = true

    }
    
    override func setupNavigationBar() {
        navigationItem.title = LocalizedKey.mealList.value
    }
    
    //Shows navigationbar only on this controller
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func observeEvents() {
        viewModel.manager.$mealList.dropFirst().receive(on: RunLoop.main).sink{ [weak self] _ in
            self?.screenView.collectionView.reloadData()
            self?.screenView.indicate = false
        }.store(in: &viewModel.bag)
    }
}

extension MealListController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       viewModel.manager.mealList.count
}

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MealListCell.identifier, for: indexPath) as! MealListCell
       cell.mealListModel = viewModel.manager.mealList[indexPath.row]
       return cell
   }

   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height*0.25)// - collectionView.bounds.width*0.9)
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        guard let meal = viewModel.manager.mealList[indexPath.row] as? MealList else {return}
        viewModel.trigger.send(AppRoute.mealDetails(meal.mealId))
        }
}

