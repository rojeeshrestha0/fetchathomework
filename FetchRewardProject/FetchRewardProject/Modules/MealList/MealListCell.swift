//
//  MealListCell.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

import Foundation
import UIKit
import Kingfisher

class MealListCell: UICollectionViewCell {
    //Image of the category
    lazy var mealImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        return imageView
    }()
    
    ///The name label of fthe category
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 0
        return label
    }()
    
    
    var mealListModel: MealList! {
        didSet {
            configureCell()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func create() {
        contentView.backgroundColor = .clear
        contentView.addSubview(mealImage)
        contentView.addSubview(nameLabel)
        
        
        NSLayoutConstraint.activate([
        ///constraint for image
            mealImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            mealImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            mealImage.heightAnchor.constraint(equalToConstant: 100.0),
            mealImage.widthAnchor.constraint(equalToConstant: 100.0),
            
            
            nameLabel.leadingAnchor.constraint(equalTo: mealImage.trailingAnchor, constant: 20.0),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            nameLabel.centerYAnchor.constraint(equalTo: mealImage.centerYAnchor)
        
        ])
    }
    
    private func configureCell() {
   
        nameLabel.text = mealListModel.mealName
        mealImage.setImage(string: mealListModel.mealImage)//"https://example.com/image.png")
    }
}
