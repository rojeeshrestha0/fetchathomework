//
//  MealListViewModel.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

import Foundation
import Combine

class MealListViewModel: BaseViewModel {
    let manager: MealCategoryManager
    let categoryName: String
    
    init(categoryName: String) {
        manager = MealCategoryManager()
        self.categoryName = categoryName
    }
    
    func callMealListApi() {
        let parameter = ["c": categoryName]
        manager.request(router: MealRouter.mealList(parameter))
    }
}
