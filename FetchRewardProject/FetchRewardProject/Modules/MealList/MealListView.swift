//
//  MealListView.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

import Foundation
import UIKit

class MealListView: BaseView {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(MealListCell.self, forCellWithReuseIdentifier: MealListCell.identifier)
        collectionView.backgroundColor = .white
        collectionView.accessibilityIdentifier = "MealListCollection"
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    override func create() {
        super.create()
        generateDesign()
    }
}

extension MealListView {
    func generateDesign() {
        addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20.0),
            collectionView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
