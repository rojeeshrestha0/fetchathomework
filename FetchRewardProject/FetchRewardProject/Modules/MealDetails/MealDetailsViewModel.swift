//
//  MealDetailsViewModel.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

import Foundation
import Combine

class MealDetailsViewModel: BaseViewModel {
    
    let manager: MealCategoryManager
    let mealId: String
    
    init(mealId: String) {
        manager = MealCategoryManager()
        self.mealId = mealId
    }
    
    func callMealListApi() {
        let parameter = ["i": mealId]
        manager.request(router: MealRouter.mealDetail(parameter))
    }
}
