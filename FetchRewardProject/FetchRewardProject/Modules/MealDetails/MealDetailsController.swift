//
//  MealDetailsController.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//


import Foundation
import UIKit
import Combine

class MealDetailsController: BaseController {
    /// The view
    private lazy var screenView: MealDetailsView = {
        return self.baseView as! MealDetailsView //swiftlint:disable:this force_cast
    }()
    
    /// The viewModel
    private lazy var viewModel: MealDetailsViewModel = {
        return self.baseViewModel as! MealDetailsViewModel //swiftlint:disable:this force_cast
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.callMealListApi()
        //screenView.indicate = true

    }
    
    override func setupNavigationBar() {
        navigationItem.title = LocalizedKey.mealRecipe.value
    }
    
    //Shows navigationbar only on this controller
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillAppear(animated)
    }
    
    func updateUIValues(details: MealDetails) {
        screenView.nameLabel.text = details.mealName
        screenView.methodDescriptionLabel.text = details.mealInstructions
    }
    
    private func createIngredientList(detail: MealDetails) {
        screenView.stackView.subviews.forEach({
            $0.removeFromSuperview()
        })
        detail.ingredients.forEach({ ingredient in
            for (key, value) in detail.measurement where key == ingredient.key {
                let view = self.createIngredientsDesign(ingredient: ingredient.value, measurement: value)
                self.screenView.stackView.addArrangedSubview(view)
                break
            }
        })
    }
    
    override func observeEvents() {
        viewModel.manager.$mealDetails.sink(receiveValue: { [weak self] details in
            guard let self = self, let details = details else { return }
            self.updateUIValues(details: details)
            self.createIngredientList(detail: details)
            
        }).store(in: &viewModel.bag)
    }
    
    private func createIngredientsDesign(ingredient: String, measurement: String) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 4.0
        let ingredientLabel = UILabel()
        ingredientLabel.translatesAutoresizingMaskIntoConstraints = false
        ingredientLabel.text = ingredient
        ingredientLabel.numberOfLines = 2
        let measurementLabel = UILabel()
        measurementLabel.translatesAutoresizingMaskIntoConstraints = false
        measurementLabel.text = measurement
        measurementLabel.numberOfLines = 2
        measurementLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        stackView.addArrangedSubview(ingredientLabel)
        stackView.addArrangedSubview(measurementLabel)
        return stackView
    }
}
