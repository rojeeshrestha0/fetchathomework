//
//  MealDetailsView.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

import Foundation
import Foundation
import UIKit

class MealDetailsView: BaseView {
    
    lazy var scrollView: UIScrollView = {
       let contentScrollView = UIScrollView()
        contentScrollView.showsVerticalScrollIndicator = false
        contentScrollView.isScrollEnabled = true
        contentScrollView.translatesAutoresizingMaskIntoConstraints = false
        return contentScrollView
    }()
    
    lazy var containingView: UIView = {
        let view = UIView()
//        view.backgroundColor = .red//
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    
    ///The name label of fthe category
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 2
        return label
    }()
    
    ///The instruction label of fthe category
    lazy var instructionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.text = LocalizedKey.instructions.value.uppercased() + "/" + LocalizedKey.measurement.value.uppercased()
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var methodLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = LocalizedKey.method.value.uppercased()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var methodDescriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
  
    
    override func create() {
        super.create()
        generateDesign()
    }
}

extension MealDetailsView {
    func generateDesign() {
        addSubview(scrollView)
        scrollView.addSubview(containingView)
        containingView.addSubview(nameLabel)
        containingView.addSubview(instructionLabel)
        containingView.addSubview(stackView)
        containingView.addSubview(methodLabel)
        containingView.addSubview(methodDescriptionLabel)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            
            containingView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containingView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containingView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containingView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containingView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            nameLabel.topAnchor.constraint(equalTo: containingView.topAnchor, constant: 10.0),
            nameLabel.leadingAnchor.constraint(equalTo: containingView.leadingAnchor, constant: 10.0),
            nameLabel.trailingAnchor.constraint(equalTo: containingView.trailingAnchor, constant: -10.0),
            
            instructionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 20.0),
            instructionLabel.leadingAnchor.constraint(equalTo: containingView.leadingAnchor, constant: 10.0),
            instructionLabel.trailingAnchor.constraint(equalTo: containingView.trailingAnchor, constant: -10.0),
            
            stackView.topAnchor.constraint(equalTo: instructionLabel.bottomAnchor, constant: 20.0),
            stackView.leadingAnchor.constraint(equalTo: containingView.leadingAnchor, constant: 10.0),
            stackView.trailingAnchor.constraint(equalTo: containingView.trailingAnchor, constant: -10.0),
            
            methodLabel.leadingAnchor.constraint(equalTo: containingView.leadingAnchor, constant: 10.0),
            methodLabel.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 30.0),
            
            methodDescriptionLabel.leadingAnchor.constraint(equalTo: methodLabel.leadingAnchor),
            methodDescriptionLabel.trailingAnchor.constraint(equalTo: containingView.trailingAnchor, constant: -10.0),
            methodDescriptionLabel.topAnchor.constraint(equalTo: methodLabel.bottomAnchor, constant: 20.0),
            methodDescriptionLabel.bottomAnchor.constraint(equalTo: containingView.bottomAnchor, constant: -10.0)
            
        ])
    }
}
