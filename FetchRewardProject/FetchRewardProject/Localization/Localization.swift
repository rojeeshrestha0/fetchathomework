//
//  Localization.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation

private protocol Localizable {
    var key: String { get }
    var value: String { get }
}

private struct Localizer {
    static func localized(key: Localizable, bundle: Bundle = .main, tableName: String = "English", value: String = "", comment: String = "", param: String = "") -> String {
        
        let value = String(format: NSLocalizedString(key.key,tableName: tableName, bundle: bundle, value: value, comment: comment), param)
        
        return value
 }
    
}

enum LocalizedKey: Localizable {
    case categoryTitle
    case mealList
    case mealRecipe
    case method
    case instructions
    case measurement
    
    var key: String {
        switch self {
        case .categoryTitle: return "CATEGORY_TITLE"
        case .mealList: return "MEAL_LIST"
        case .mealRecipe: return "MEAL_RECIPE"
        case .method: return "METHOD"
        case .instructions: return "INSTRUCTIONS"
        case .measurement: return "MEASUREMENT"
        }
    }
    
    var value: String {
        switch self {
        default:
            return Localizer.localized(key: self)
        }
    }
    
    
}
