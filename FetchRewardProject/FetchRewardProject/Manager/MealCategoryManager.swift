//
//  MealCategoryManager.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import Combine

typealias Params = [String: Any]
class MealCategoryManager {
    var bag = Set<AnyCancellable>()
    @Published var categories: [MealCategory] = []
    @Published var mealList: [MealList] = []
    @Published var mealDetails: MealDetails?

    
    let isApicall = PassthroughSubject<Bool,Never>()
    let networking = Networking()
    
    func request(router: Routable) {
        isApicall.send(true)
        networking.request(router: router).sink { [weak self] result in
            guard let self = self else {  return }
            self.isApicall.send(false)
            self.parseResult(result: result, router: router)
        }.store(in: &bag)
    }
    
    private func parseResult(result: NetworkingResult, router: Routable)  {
        switch router {
        case MealRouter.categories:
            if !result.object.isEmpty,
               let categoryJson = result.object["categories"] as? [Params]
            {
                let categories = DataMapper<MealCategory>.arrayMapping(categoryJson)
                print(categories)
                self.categories =  categories.sorted(by: {$0.categoryName < $1.categoryName})
            }
            break
        case MealRouter.mealList:
            if !result.object.isEmpty,
               let mealListJson = result.object["meals"] as? [Params]
            {
                let mealList = DataMapper<MealList>.arrayMapping(mealListJson)
                print(mealList)
                self.mealList =  mealList.sorted(by: {$0.mealName < $1.mealName})
            }
        case MealRouter.mealDetail:
            if !result.object.isEmpty,
               let mealJson = result.object["meals"] as? [Params]
            {
                var mealDetail = DataMapper<MealDetails>.arrayMapping(mealJson).first
                mealDetail?.builder(json: mealJson[0])
                print(mealDetail)
                self.mealDetails = mealDetail
            }
             
            break
        default:
            break
        }

    }
    
}
