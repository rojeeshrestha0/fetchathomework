//
//  AppCoordinator.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/29/22.
//

import Foundation
import UIKit
import Combine

final class AppCoordinator: BaseCoordinator {
    /// The main route of the app
    private let route: Route
    
    ///the dispose bag
    var bag = Set<AnyCancellable>()
     
    init(route: Route) {
        self.route = route
        
        super.init()
    }
    
    override func start(with deepLink: Deeplink?) {
       runHomeCoordinator()
    }
    
    ///to run teh main home coordinator
    private func runHomeCoordinator() {
        let homeCoordinator = CategoryCoordinator(route: route)
            homeCoordinator.onFinish = {[weak self] in
                guard let self = self else { return }
                self.start(with: nil)
            }
            coordinate(to: homeCoordinator)
    }

}
