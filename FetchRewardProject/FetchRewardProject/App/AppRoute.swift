//
//  AppRoute.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation

enum AppRoute: AppRoutable {
    case category
    case mealList(String)
    case mealDetails(String)
}
