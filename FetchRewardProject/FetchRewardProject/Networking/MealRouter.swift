//
//  MealRouter.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import Alamofire
import SwiftUI

enum MealRouter: Routable {
    case categories
    case mealList(Parameters)
    case mealDetail(Parameters)
    
    var path: String {
        switch self {
        case .categories:
            return "categories.php"
        case .mealList:
            return "filter.php"
        case .mealDetail:
            return "lookup.php"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: MealRouter.baseUrl.appendingPathComponent(path))
        urlRequest.httpMethod = httpMethod.rawValue
        headers.forEach{ urlRequest.addValue($0.value, forHTTPHeaderField: $0.key)}
        switch self {
        case .mealList(let parameter), .mealDetail(let parameter):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameter)
            break
        case .categories:
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: nil)
            break
        }
        return urlRequest
    }
    
}
