//
//  BaseCoordinator.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/29/22.
//

import Foundation
import UIKit

public protocol Coordinator: AnyObject {
    func start(with deepLink: Deeplink?)
}

open class BaseCoordinator: NSObject, Coordinator {
    // Private property used to hols refrence to the child coordinator
    private(set) var childCoordinators: [Coordinator] = []
    
    // Closure to indicate the removal of the coordinator
    var coordinatorRemoved:  (() -> Void)?
    
    //When coordinator is removed
    private var onRemove: (() -> Void)?
    
    //When a coordinator is finished
    public var onFinish: (() -> Void)?
    
    //public initializer
    public override init(){}
    
    
    /// Method to start coordinator with deeplink
    ///
    /// - Parameter deeplink: Deeplink
    public func start(with deepLink: Deeplink?) {}
    
    
    private func addChild(_ coordinator: Coordinator) {
        guard !childCoordinators.contains(where: { $0 === coordinator}) else { return } //child coordinator is already coordinating check
        childCoordinators.append(coordinator)
    }
    
    private func removeChild(_ coordinator: Coordinator) {
        guard !childCoordinators.isEmpty, let coordinator = coordinator as? BaseCoordinator else { return }
        
        // Remove all the child of the coordinators
        if !coordinator.childCoordinators.isEmpty {
            coordinator.childCoordinators.filter({$0 !== coordinator})
                .forEach({coordinator.removeChild($0)})
        }
        
        // Removing the coordinator
        for (index, element) in childCoordinators.enumerated() where element ===
        coordinator {
            childCoordinators.remove(at: index)
            break
        }
        
        //det coordinator was removed
        coordinatorRemoved?()
    }
    
    ///Function that adds new coordinator and starts it
    ///
    ///- Parameter coordinator
    public func coordinate(to coordinator: BaseCoordinator, deepLink: Deeplink? = nil) {
        addChild(coordinator)
        coordinator.onRemove = {[unowned self, unowned coordinator] in
            self.removeChild(coordinator)
        }
        coordinator.start(with: deepLink)
    }
    
    ///Method to complete the operation o fthe coordinator
    public func finish() {
        onRemove?()
        onFinish?()
    }
    
    public func removeOnly() {
        onRemove?()
    }
    
    /// returns child coordinator of type C if it exists. else return nil
    /// - Parameter type: class of type Coordinator
    public func getChild<C>(type: C.Type) -> C? {
        for coordinator in getChildrens() {
            if let requiredCoordinator = coordinator as? C {
                return requiredCoordinator
            }
        }
        return nil
    }
    
    ///returns all child coordinators
    ///- Returns: child Coordinators
    public func getChildrens() -> [Coordinator] {
        return childCoordinators
    }
    
    deinit{
        print("Deinit ->", String(describing: self))
    }
    
    
}
