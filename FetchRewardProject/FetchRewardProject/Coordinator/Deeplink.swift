//
//  Deeplink.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/29/22.
//

import  Foundation

/// The deeplink here is usd to navigate to various portion of the app
/// This will help identify the deeplink within the app

public protocol Deeplink { }
