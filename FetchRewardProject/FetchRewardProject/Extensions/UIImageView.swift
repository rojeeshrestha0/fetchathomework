//
//  UIImageView.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(string: String) {
        if let url = URL(string: string) {
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url)
            self.kf.setImage(with: url)

        }
    }
    
}
