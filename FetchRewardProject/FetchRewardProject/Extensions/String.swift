//
//  String.swift
//  FetchRewardProject
//
//  Created by Rosy on 2/1/22.
//

import Foundation
extension String {
    var stripped: String {
           let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890.-")
           return self.filter {okayChars.contains($0) }
        
            //let not
       
       }
    
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}
