//
//  UIView.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

import Foundation
import UIKit


extension UIView {
    
    /// The name of the view
    static var identifier: String {
        return String(describing: self)
    }
}
