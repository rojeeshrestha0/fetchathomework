//
//  MealCategory.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/30/22.
//

//{"categories":[{"idCategory":"1","strCategory":"Beef","strCategoryThumb":"https:\/\/www.themealdb.com\/images\/category\/beef.png","strCategoryDescription":"Beef is the culinary name for meat from cattle, particularly skeletal muscle. Humans have been eating beef since prehistoric times.[1] Beef is a source of high-quality protein and essential nutrients.[2]"},...
import Foundation

struct MealCategory: Codable {
    var categoryName: String = ""
    var categoryImage: String = ""
    var categoryDescription: String = ""
    
    
    enum CodingKeys: String, CodingKey {
        case categoryName = "strCategory"
        case categoryImage = "strCategoryThumb"
        case categoryDescription = "strCategoryDescription"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        categoryName = try container.decodeIfPresent(String.self, forKey: .categoryName) ?? ""
        categoryImage = try container.decodeIfPresent(String.self, forKey: .categoryImage) ?? ""
        categoryDescription = try container.decodeIfPresent(String.self, forKey: .categoryDescription) ?? ""

    }
    
}
