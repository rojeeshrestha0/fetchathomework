//
//  MealList.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//


//{"meals":[{"strMeal":"Beef and Mustard Pie","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/sytuqu1511553755.jpg","idMeal":"52874"}
import Foundation

struct MealList: Codable {
    var mealName: String = ""
    var mealImage: String = ""
    var mealId: String = ""
    
    
    enum CodingKeys: String, CodingKey {
        case mealName = "strMeal"
        case mealImage = "strMealThumb"
        case mealId = "idMeal"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        mealName = try container.decodeIfPresent(String.self, forKey: .mealName) ?? ""
        mealImage = try container.decodeIfPresent(String.self, forKey: .mealImage) ?? ""
        mealId = try container.decodeIfPresent(String.self, forKey: .mealId) ?? ""

    }
    
}
