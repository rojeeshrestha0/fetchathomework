//
//  MealDetails.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/31/22.
//

//{"meals":[{"idMeal":"52959","strMeal":"Baked salmon with fennel & tomatoes","strDrinkAlternate":null,"strCategory":"Seafood","strArea":"British","strInstructions":"Heat oven to 180C\/fan 160C\/gas 4. Trim the fronds from the fennel and set aside. Cut the fennel bulbs in half, then cut each half into 3 wedges. Cook in boiling salted water for 10 mins, then drain well. Chop the fennel fronds roughly, then mix with the parsley and lemon zest.\r\n\r\nSpread the drained fennel over a shallow ovenproof dish, then add the tomatoes. Drizzle with olive oil, then bake for 10 mins. Nestle the salmon among the veg, sprinkle with lemon juice, then bake 15 mins more until the fish is just cooked. Scatter over the parsley and serve.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/1548772327.jpg","strTags":"Paleo,Keto,HighFat,Baking,LowCarbs","strYoutube":"https:\/\/www.youtube.com\/watch?v=xvPR2Tfw5k0","strIngredient1":"Fennel","strIngredient2":"Parsley","strIngredient3":"Lemon","strIngredient4":"Cherry Tomatoes","strIngredient5":"Olive Oil","strIngredient6":"Salmon","strIngredient7":"Black Olives","strIngredient8":"","strIngredient9":"","strIngredient10":"","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"2 medium","strMeasure2":"2 tbs chopped","strMeasure3":"Juice of 1","strMeasure4":"175g","strMeasure5":"1 tbs","strMeasure6":"350g","strMeasure7":"to serve","strMeasure8":"","strMeasure9":"","strMeasure10":"","strMeasure11":"","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/7745\/baked-salmon-with-fennel-and-tomatoes","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]}
import Foundation
struct MealDetails: Codable {
    var mealName: String = ""
    var mealInstructions: String = ""
    var ingredients: [String: String] = [:]
    var measurement: [String: String] = [:]
    
    
    enum CodingKeys: String, CodingKey {
        case mealName = "strMeal"
        case mealInstructions = "strInstructions"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        mealName = try container.decodeIfPresent(String.self, forKey: .mealName) ?? ""
        mealInstructions = try container.decodeIfPresent(String.self, forKey: .mealInstructions) ?? ""
        
    }
    
    mutating func builder(json: [String: Any?]) {
        
        json.forEach { dic in
            if  dic.key.hasPrefix("strIngredient"), let value = dic.value as? String, !value.isEmpty {
                let afterRemoval = dic.key.deletingPrefix("strIngredient")
                ingredients[afterRemoval] = value
            }
        }
       // let ingredient = ingredients.sorted(by: {$0.key < $1.key}).flatMap({$0})
        
        json.forEach { dic in
            if  dic.key.hasPrefix("strMeasure"), let value = dic.value as? String, !value.isEmpty {
                let afterRemoval = dic.key.deletingPrefix("strMeasure")
                measurement[afterRemoval] = value
            }
            
        }
    }
    
    
    
}
