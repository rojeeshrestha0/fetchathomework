//
//  AlertActionable.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/29/22.
//

import Foundation
import UIKit

public protocol AlertActionable {
    var title: String { get }
    var style: UIAlertAction.Style { get }
}


