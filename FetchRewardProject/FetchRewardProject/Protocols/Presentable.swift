//
//  Presentable.swift
//  FetchRewardProject
//
//  Created by Rosy on 1/29/22.
//

import Foundation
import UIKit

/// The presentable protocol for coordinators
public protocol Presentable {
    var presenting: UIViewController? { get }
}
